from rest_framework.routers import DefaultRouter
from .views import JogosViewSets

router = DefaultRouter()
router.register(r'jogos', JogosViewSets)

urlpatterns = router.urls
