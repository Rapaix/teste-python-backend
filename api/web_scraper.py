import cfscrape
from bs4 import BeautifulSoup


def get_result():

    URL = "https://www.google.com/search?q=caixa+mega+sena"

    scraper = cfscrape.create_scraper()
    html = scraper.get(URL).content
    soup = BeautifulSoup(html, "lxml")

    span_list = [x.extract() for x in soup.find_all('span')]
    dezenas_vencedoras = []

    for span in span_list:
        if span.text is not None and span.text.isnumeric() and len(span.text) == 2:
            dezenas_vencedoras.append(int(span.text))

    print(dezenas_vencedoras)
    return dezenas_vencedoras


get_result()
