from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers 
from .models import Jogos

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)

        token['username'] = user.username
        return token


class DezenasSerializer(serializers.Serializer):

    dezenas = serializers.IntegerField(min_value=6, max_value=10)


class JogosSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Jogos
        fields = ['id', 'dezenas', 'created']

    dezenas = serializers.ListField(
        child=serializers.IntegerField())
