from rest_framework import status, viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response
from django.shortcuts import render
from .models import Jogos
from .serializers import JogosSerializer, DezenasSerializer, MyTokenObtainPairSerializer
from random import sample
from .web_scraper import get_result
from rest_framework_simplejwt.views import TokenObtainPairView

# Create your views here.


class JogosViewSets(viewsets.ViewSet):

    queryset = Jogos.objects.all()
    serializer = JogosSerializer
    permission_classes = [IsAuthenticated]

    def gerar_dezenas(self, numero_dezenas):

        dezenas = sample(range(1, 60), numero_dezenas)

        return dezenas

    def create(self, request):
        serializer = DezenasSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        j = Jogos.objects.create(dezenas=self.gerar_dezenas(
            numero_dezenas=request.data['dezenas']), user=request.user)

        serializer = JogosSerializer(j)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request):

        lista_jogos = Jogos.objects.filter(user=request.user)

        if not len(lista_jogos):
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = JogosSerializer(lista_jogos, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):

        try:
            jogo = Jogos.objects.get(id=pk, user=request.user)
            serializer = JogosSerializer(jogo)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @action(detail=True, methods=['GET'])
    def result(self, request, pk=None):
        try:
            jogo = Jogos.objects.get(id=pk, user=request.user)

            dezenas_vencedoras = get_result()
            print(dezenas_vencedoras)
            response_data = {
                "Dezenas Vencedoras": dezenas_vencedoras,
                "Dezena do seu Jogo": jogo.dezenas,
                "Dezenas Acertadas": set(jogo.dezenas).intersection(
                    set(dezenas_vencedoras))
            }

            return Response(response_data, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    @action(detail=False, methods=['GET'])
    def megasena(self, request):
        try:
            return Response('megasena', status=status.HTTP_200_OK)
        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND)


