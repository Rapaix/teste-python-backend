from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User
from django.db import models
# Create your models here.


class Jogos(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='jogos',
        related_query_name='jogo')
    dezenas = ArrayField(models.IntegerField(blank=True, null=False),
                         max_length=10, blank=False, null=False)
    created = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"Dezenas:  {str(self.dezenas)}"

    class Meta:
        ordering = ['created']
        verbose_name_plural = "Jogos"
